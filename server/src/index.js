import express from "express";
import cors from "cors";
import path from "path";

const app = express();

const __dirname = path.resolve();

app.use(
  cors({
    origin: "*",
  })
);

const URL_TOP_MOVIES = "./public/data.json";

app.get("/topmovies", async (_, res) => {
  try {
    const file = path.join(__dirname, URL_TOP_MOVIES);
    res.sendFile(file);
  } catch (e) {
    console.error(e.message);
  }
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.info("listening on " + port));

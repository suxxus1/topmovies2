import * as React from "react";
import type { Preview } from "@storybook/react";
import { GlobalStyles } from ".././src/styles/globalStyles";
import { withRouter } from "storybook-addon-react-router-v6";
import { FakeMovies } from "../src/mocks";
import {
  CardsContextProvider,
  PaginationContextProvider,
  FetchDataMoviesContextProvider,
  SearchBoxContextProvider,
} from "../src/context";

const fetchMock = {
  debug: true,
  catchAllMocks: [{ matcher: { url: "path:/topmovies" }, response: 200 }],
  mocks: [
    {
      matcher: {
        name: "topmovies",
        url: "path:/topmovies",
      },
      response: {
        status: 200,
        body: {
          entries: [...FakeMovies.entries],
        },
      },
    },
  ],
};

/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
const DefaultDecorator = (Story: any) => {
  return (
    <>
      <GlobalStyles />
      <FetchDataMoviesContextProvider>
        <CardsContextProvider>
          <PaginationContextProvider>
            <SearchBoxContextProvider>
              <Story />
            </SearchBoxContextProvider>
          </PaginationContextProvider>
        </CardsContextProvider>
      </FetchDataMoviesContextProvider>
    </>
  );
};

const decorators = [DefaultDecorator, withRouter];

const preview: Preview = {
  parameters: {
    fetchMock,
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
  decorators,
};

export default preview;

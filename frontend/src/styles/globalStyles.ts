import { createGlobalStyle, css } from "styled-components";
import resetCss from "./resetcss";

const font = `
  font-family: 'Open Sans', sans-serif;
  font-size: 16px;
  font-synthesis: none;
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  -webkit-text-size-adjust: 100%;
`;

const commonVariables = `
  --color-default: #fff;
  --top-bottom-bg: #000000;
  --red-bg-50: #c42521;
  --red-bg-60: #80201e;
  --primary-bg: #1a1a1a;
  --placeholder: #999;
`;

const lightTheme = `
    ${commonVariables}
  `;

const darkTheme = `
    ${commonVariables}
`;

const global = css`
  ${resetCss}
  :root {
    ${font}
    ${(props) => {
      return props.theme?.isDark ? darkTheme : lightTheme;
    }}
  }

  body {
    color: var(--color-default);
    background-color: #0d1117;
    min-height: 100vh;
  }

  a {
    color: unset;
  }

  button {
    cursor: pointer;
  }
`;

export const GlobalStyles = createGlobalStyle`
    ${global}
`;

const size = {
  mobileS: "320px",
  mobileM: "375px",
  mobileL: "425px",
  tablet: "768px",
  laptop: "1024px",
  laptopL: "1440px",
  desktop: "2560px",
};

export const device = {
  mobileS: `(min-width: ${size.mobileS})`,
  mobileM: `(min-width: ${size.mobileM})`,
  mobileL: `(min-width: ${size.mobileL})`,
  tablet: `(min-width: ${size.tablet})`,
  laptop: `(min-width: ${size.laptop})`,
  laptopL: `(min-width: ${size.laptopL})`,
  desktop: `(min-width: ${size.desktop})`,
  desktopL: `(min-width: ${size.desktop})`,
};

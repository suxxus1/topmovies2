import { Cards } from "../types";
import { MovieData } from "../utils/jsonDecoder";
import topmovies from "./topmovies.json";

export const defaultEntry: MovieData = {
  title: "",
  description: "",
  releaseYear: 0,
  images: {
    posterArt: {
      url: "",
    },
  },
};

type MoviesDto = {
  total: number;
  entries: MovieData[];
};

export const FakeMovies: MoviesDto = Object.freeze({
  total: 49,
  entries: [...topmovies.entries],
});

export function getFakeMovies(
  startIndex: number,
  endIndex: number,
  movies: MoviesDto
) {
  return {
    ...movies,
    entries: movies.entries.slice(startIndex, endIndex),
  };
}

export function getFetchMock(entries: unknown[]) {
  return {
    debug: true,
    catchAllMocks: [{ matcher: { url: "path:/topmovies" }, response: 200 }],
    mocks: [
      {
        matcher: {
          name: "topmovies",
          url: "path:/topmovies",
        },
        response: {
          status: 200,
          body: {
            entries,
          },
        },
      },
    ],
  };
}

export const FakeState: Cards = {
  ids: ["1", "2", "12"],
  data: {
    "1": {
      id: "1",
      title: "American History X",
      description:
        "A former neo-nazi skinhead tries to prevent his younger brother from going down the same wrong path that he did.",
      image:
        "https://static.rviewer.io/challenges/datasets/dreadful-cherry-tomatoes/assets/american_history_x.jpg",
      releaseYear: 1998,
    },
    "2": {
      id: "2",
      title: "Pirates of the Caribbean: At World's End",
      description:
        "The true story of Frank Abagnale Jr. who, before his 19th birthday, successfully conned millions of dollars' worth of checks as a Pan Am pilot, doctor, and legal prosecutor.Fringilla ut morbi tincidunt augue interdum velit. Id leo in vitae turpis massa sed elementum tempus",
      image:
        "https://static.rviewer.io/challenges/datasets/dreadful-cherry-tomatoes/assets/black_swan.jpg",
      releaseYear: 2002,
    },
    "12": {
      id: "12",
      title: "Pirates of the Caribbean: X",
      description:
        "The true story of Frank Abagnale Jr. who, before his 19th birthday, successfully conned millions of dollars' worth of checks as a Pan Am pilot, doctor, and legal prosecutor.Fringilla ut morbi tincidunt augue interdum velit. Id leo in vitae turpis massa sed elementum tempus",
      image:
        "https://static.rviewer.io/challenges/datasets/dreadful-cherry-tomatoes/assets/catch_me_if_you_can.jpg",
      releaseYear: 2002,
    },
  },
};

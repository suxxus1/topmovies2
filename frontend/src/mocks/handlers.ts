import { rest } from "msw";
import topMovies from "./topmovies.json";

const ok = 200;

export const handlers = [
  rest.get("/topmovies", (_, res, ctx) =>
    res(ctx.status(ok), ctx.json({ ...topMovies }))
  ),
];

import React, { useEffect } from "react";
import { render, screen } from "../components/testUtils";
import userEvent from "@testing-library/user-event";
import { usePaginationContext } from "./";
import constants from "../constants";
import { CardId } from "../types";

function Page({ ids }: { ids: CardId[] }): JSX.Element {
  const { pageNum, pageSelected, setPageSeleted, setPageNum } =
    usePaginationContext();

  const numberTwo = 2;
  const one = 1;

  useEffect(() => {
    setPageNum(Math.ceil(ids.length / constants.MAX_CARDS_PER_PAGE));
  }, [setPageNum, ids.length]);

  return (
    <>
      <ul>
        {Array(pageNum)
          .fill("")
          .map((_, i) => (
            <li
              key={i}
              data-testid={`num-${i + one}${
                pageSelected === i + one ? "-selected" : ""
              }`}
            >
              {i + one}
            </li>
          ))}
      </ul>
      <button
        onClick={() => {
          setPageSeleted(numberTwo);
        }}
      >
        two
      </button>
    </>
  );
}

describe("<CardsContextProvider />", () => {
  const upToTwenty = 20;

  const ids = Array(upToTwenty)
    .fill("")
    .map((_, i) => i.toString());

  test("Render correctly", () => {
    render(<Page ids={ids} />, {});

    screen.getByText("1");
    screen.getByText("2");
  });

  test("User click on Page number 2", async () => {
    const testId = "num-2-selected";

    render(<Page ids={ids} />, {});

    expect(screen.queryByTestId(testId)).not.toBeInTheDocument();

    await userEvent.click(screen.getByText("two"));

    screen.getByTestId(testId);
  });
});

import React, { useContext, createContext } from "react";
import { useFetch } from "usehooks-ts";
import { Maybe, Nothing, Children } from "../types";
import { MovieData } from "../utils/jsonDecoder";
import constants from "../constants";
import { moviesDataDecoder } from "../utils/jsonDecoder";

type FetchDataMovie = {
  isLoading: boolean;
  data: Maybe<MovieData[]>;
};

const FetchDataMoviesContext = createContext<FetchDataMovie>({
  isLoading: false,
  data: Nothing,
});

function MovieDataContextProvider({ children }: Children): JSX.Element {
  const { data, error } = useFetch<unknown>(constants.URL_TOP_MOVIES);

  if (error) {
    console.error(`on json => ${error.message}`);
  }

  return (
    <FetchDataMoviesContext.Provider
      value={{
        isLoading: error ? false : !data,
        data: !data ? Nothing : moviesDataDecoder(data),
      }}
    >
      {children}
    </FetchDataMoviesContext.Provider>
  );
}

export function useFetchDataMoviesContext(): FetchDataMovie {
  const contextValue = useContext(FetchDataMoviesContext);

  const contextName = "FetchDataMoviesContext";
  const { CONTEXT_ERROR_MSG } = constants;

  if (!contextValue) throw new Error(`${contextName}${CONTEXT_ERROR_MSG}`);

  return contextValue;
}

export default MovieDataContextProvider;

import React from "react";
import { render, screen } from "../components/testUtils";
import { useCardsContext } from "./";
import { FakeMovies } from "../mocks/";

function Page(): JSX.Element {
  const { ids, data } = useCardsContext();

  return (
    <ul>
      {ids.map((id) => (
        <li key={id}>{data[id].title}</li>
      ))}
    </ul>
  );
}

describe("<CardsContextProvider />", () => {
  test("Render correctly", () => {
    const first = 0;
    const second = 1;

    render(<Page />, { data: FakeMovies });
    screen.getByText(FakeMovies.entries.at(first)?.title || "");
    screen.getByText(FakeMovies.entries.at(second)?.title || "");
  });
});

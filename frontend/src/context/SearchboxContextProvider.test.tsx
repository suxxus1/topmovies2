import React, { useEffect } from "react";
import { render, screen } from "../components/testUtils";
import { useSearchBoxContext } from "./";

function Page({ search }: { search: string }): JSX.Element {
  const { searchValue, setSearchValue } = useSearchBoxContext();
  useEffect(() => {
    setSearchValue(search);
  }, [searchValue, setSearchValue, search]);

  return <p>{searchValue}</p>;
}

describe("<SearchBoxContextProvider />", () => {
  test("Search result", () => {
    render(<Page search={""} />, {});
    expect(screen.queryByText(/to be/)).not.toBeInTheDocument();
  });

  test("Search result xyz", () => {
    render(<Page search="xyz" />, {});
    screen.queryByText(/xyz/);
  });
});

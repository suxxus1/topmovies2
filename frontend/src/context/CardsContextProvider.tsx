import React, { useContext, createContext } from "react";
import { Cards, CardId, Children, Nothing } from "../types";
import { MovieData } from "../utils/jsonDecoder";
import constants from "../constants";
import {
  sortByYear,
  transformMovieData,
  getOddEvenOrderedList,
  getChunks,
} from "../utils";

import { useFetchDataMoviesContext } from "./";

const CardsContext = createContext<Cards>({
  ids: [],
  data: {},
});

function getCards(moviesData: MovieData[]): Cards {
  const { ids, data } = sortByYear(transformMovieData(moviesData));
  return {
    ids: getOddEvenOrderedList(
      getChunks(ids, constants.CARDS_PER_ROW)
    ) as CardId[],
    data,
  };
}

function CardsContextProvider({ children }: Children): JSX.Element {
  const { data } = useFetchDataMoviesContext();
  const movieData = data !== Nothing ? data : [];

  return (
    <CardsContext.Provider
      value={{
        ...getCards(movieData),
      }}
    >
      {children}
    </CardsContext.Provider>
  );
}

export function useCardsContext(): Cards {
  const contextValue = useContext(CardsContext);

  const contextName = "CardsContext";
  const { CONTEXT_ERROR_MSG } = constants;

  if (!contextValue) throw new Error(`${contextName}${CONTEXT_ERROR_MSG}`);

  return contextValue;
}

export default CardsContextProvider;

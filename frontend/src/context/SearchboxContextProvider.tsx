import React, { useState, useContext, createContext } from "react";
import { SearchBox, Children } from "../types";
import constants from "../constants";

export const SearchBoxContext = createContext<SearchBox>({
  searchValue: "",
  setSearchValue: (a) => a,
});

function SearchBoxContextProvider({ children }: Children): JSX.Element {
  const [searchValue, setSearchValue] = useState<string>("");

  return (
    <SearchBoxContext.Provider
      value={{
        searchValue,
        setSearchValue,
      }}
    >
      {children}
    </SearchBoxContext.Provider>
  );
}

export function useSearchBoxContext(): SearchBox {
  const contextValue = useContext(SearchBoxContext);

  const contextName = "SearchBoxContext";
  const { CONTEXT_ERROR_MSG } = constants;

  if (!contextValue) throw new Error(`${contextName}${CONTEXT_ERROR_MSG}`);

  return contextValue;
}

export default SearchBoxContextProvider;

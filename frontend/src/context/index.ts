import CardsContextProvider, { useCardsContext } from "./CardsContextProvider";

import PaginationContextProvider, {
  usePaginationContext,
} from "./PaginationContextProvider";

import SearchBoxContextProvider, {
  useSearchBoxContext,
} from "./SearchboxContextProvider";

import FetchDataMoviesContextProvider, {
  useFetchDataMoviesContext,
} from "./FetchMovieDataContextProvider";

export {
  FetchDataMoviesContextProvider,
  useFetchDataMoviesContext,
  useCardsContext,
  CardsContextProvider,
  PaginationContextProvider,
  usePaginationContext,
  SearchBoxContextProvider,
  useSearchBoxContext,
};

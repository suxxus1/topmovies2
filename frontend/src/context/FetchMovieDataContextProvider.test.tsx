import React from "react";
import { render, screen } from "../components/testUtils";
import { useFetchDataMoviesContext } from "./";
import { FakeMovies } from "../mocks/";
import { Nothing } from "../types";

function Page(): JSX.Element {
  const { data } = useFetchDataMoviesContext();
  if (data !== Nothing) {
    return (
      <ul>
        {data.map((movie, i) => (
          <li key={`movie-${i}`}>{movie.title}</li>
        ))}
      </ul>
    );
  }
  //
  return <>Nothing</>;
}

describe("<CardsContextProvider />", () => {
  test("Render correctly", () => {
    const first = 0;
    const second = 1;
    render(<Page />, { data: FakeMovies });
    screen.getByText(FakeMovies.entries.at(first)?.title || "");
    screen.getByText(FakeMovies.entries.at(second)?.title || "");
  });
});

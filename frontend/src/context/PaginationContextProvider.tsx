import React, { useContext, createContext, useState } from "react";
import { Children, Pagination } from "../types";
import constants from "../constants";

const PaginationContext = createContext<Pagination>({
  setPageSeleted: (a) => a,
  pageNum: 0,
  setPageNum: (a) => a,
  pageSelected: 0,
});

function PaginationContextProvider({ children }: Children): JSX.Element {
  const initialPageNum = 0;
  const [pageSelected, setPageSeleted] = useState<number>(
    constants.STARTING_PAGE
  );

  const [pageNum, setPageNum] = useState<number>(initialPageNum);

  return (
    <PaginationContext.Provider
      value={{
        pageSelected,
        setPageSeleted,
        setPageNum,
        pageNum,
      }}
    >
      {children}
    </PaginationContext.Provider>
  );
}

export function usePaginationContext(): Pagination {
  const contextValue = useContext(PaginationContext);

  const contextName = "PaginationContext";
  const { CONTEXT_ERROR_MSG } = constants;

  if (!contextValue) throw new Error(`${contextName}${CONTEXT_ERROR_MSG}`);

  return contextValue;
}

export default PaginationContextProvider;

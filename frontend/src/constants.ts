export default Object.freeze({
  STARTING_PAGE: 1,
  MAX_CARDS_PER_PAGE: 10,
  CARDS_PER_ROW: 5,
  SEARCH_CRITERIA_MIN_LENGTH: 1,
  URL_TOP_MOVIES: `${process.env.BASE_URL || ""}/topmovies`,

  NOT_VALID_JSON_MSG: "Data errors",
  CONTEXT_ERROR_MSG: " was used outside of its Provider",
});

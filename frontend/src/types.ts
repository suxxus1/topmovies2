export const Nothing = Symbol("nothing");
type Nothing = typeof Nothing;

export type Maybe<T> = T | Nothing;

export type Children = {
  children: React.ReactNode;
};

export type CardId = string;

export type Dict<T> = Record<string, T>;

export type MovieCard = {
  id: string;
  image: string;
  title: string;
  description: string;
  releaseYear: number;
};

export type ErroMsg = {
  status: "Ok" | "Ko";
  message: string;
};

export type JsonError = {
  error: ErroMsg;
  setErrorMsg: React.Dispatch<React.SetStateAction<ErroMsg>>;
};

export type Cards = {
  ids: CardId[];
  data: Dict<MovieCard>;
};

export type Chevron = "goRight" | "goLeft";

export type SearchBox = {
  setSearchValue: React.Dispatch<React.SetStateAction<string>>;
  searchValue: string;
};

export type Pagination = {
  setPageSeleted: React.Dispatch<React.SetStateAction<number>>;
  pageSelected: number;
  setPageNum: React.Dispatch<React.SetStateAction<number>>;
  pageNum: number;
};

export type SearchByTitle = {
  title: string;
  id: string;
};

export type Loading = {
  isLoading: boolean;
  setIsLoading: React.Dispatch<React.SetStateAction<boolean>>;
};

import { Cards, CardId, SearchByTitle } from "../types";

export function getCloned(value: unknown): unknown {
  return JSON.parse(JSON.stringify(value));
}

export function getChunks(xs: unknown[], chunk_size: number): unknown[] {
  /* eslint-disable no-magic-numbers */
  return xs
    .map((_, i) => (i % chunk_size === 0 ? xs.slice(i, i + chunk_size) : null))
    .filter((e) => e);
}

/**
 * get a list sorted according to the index of the elements,
 * the even indexes are placed after the odd ones
 *
 * eg ["a", "b", "c", "d"] -> ["a", "c", "b", "d"]
 *
 * @param {Array} xs list to be ordered
 * @return {Array}
 *
 */
export function getOddEvenOrderedList(xs: unknown[]): unknown[] {
  /* eslint-disable no-magic-numbers */
  const odd = xs.filter((_, i) => i % 2 !== 1);
  const even = xs.filter((_, i) => i % 2);
  return [...odd, ...even].flat();
}

// components helpers
// ==================

export function sortByYear(cards: Cards): Cards {
  return {
    ids: cards.ids
      .map((id) => cards.data[id])
      .sort((a, b) => b.releaseYear - a.releaseYear)
      .map((item) => item.id),
    data: {
      ...cards.data,
    },
  };
}

export function getValidCards(cards: Cards): Cards {
  return {
    ids: cards.ids.filter((id) => cards.data[id]),
    data: cards.data,
  };
}

export function createSearchList(cards: Cards): SearchByTitle[] {
  return getValidCards(cards).ids.map((id) => ({
    title: cards.data[id].title,
    id,
  }));
}

export function getSearchResult(
  searchList: SearchByTitle[],
  searchCriteria: string
): CardId[] {
  const regex = new RegExp(searchCriteria.split("").join(".*"), "i");

  return searchList
    .filter(({ title }) => regex.test(title))
    .map(({ id }) => id);
}

type OrderedCardsProps = {
  cards: Cards;
  searchList: SearchByTitle[];
  searchValue: string;
  chunksNumber: number;
  isSearch: boolean;
};

export function getOrderedCards({
  cards,
  searchList,
  searchValue,
  chunksNumber,
  isSearch,
}: OrderedCardsProps): {
  ids: CardId[];
} {
  if (isSearch) {
    return {
      ids: getOddEvenOrderedList(
        getChunks(
          sortByYear({
            ids: getSearchResult(searchList, searchValue),
            data: cards.data,
          }).ids,
          chunksNumber
        )
      ) as CardId[],
    };
  }

  return {
    ids: [...cards.ids],
  };
}

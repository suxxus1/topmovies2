import {
  sortByYear,
  createSearchList,
  getSearchResult,
  getChunks,
  getOddEvenOrderedList,
  getOrderedCards,
} from "./";
import { Cards, SearchByTitle } from "../types";
import { FakeState } from "../mocks";

const card = {
  title: "lorem",
  description: "",
  image: "",
};

const cards: Cards = {
  ids: ["1", "2", "3"],
  data: {
    "1": {
      ...card,
      id: "1",
      releaseYear: 2002,
    },
    "2": {
      ...card,
      id: "2",
      releaseYear: 2009,
    },
    "3": {
      ...card,
      id: "3",
      releaseYear: 1998,
    },
  },
};

describe("Helpers", () => {
  test("Given Cards, should return Cards ids sorted by year", () => {
    const actual = cards.ids;

    expect(actual).toStrictEqual(["1", "2", "3"]);

    const expected = {
      ids: ["2", "1", "3"],
      data: {
        ...cards.data,
      },
    };

    const result = sortByYear(cards);

    expect(result.ids).toStrictEqual(expected.ids);
    expect(result.data).toStrictEqual(expected.data);
  });

  test("Given Cards should return SearchByTilte list", () => {
    const actual = {
      ids: ["1", "2", "3"],
      data: {
        "1": {
          ...card,
          id: "1",
          releaseYear: 2002,
          title: "one",
        },
        "2": {
          ...card,
          id: "2",
          releaseYear: 2009,
          title: "two",
        },
        "3": {
          ...card,
          id: "3",
          releaseYear: 1998,
          title: "three",
        },
      },
    };

    const expected: SearchByTitle[] = [
      {
        title: "one",
        id: "1",
      },
      { title: "two", id: "2" },
      { title: "three", id: "3" },
    ];

    expect(createSearchList(actual)).toStrictEqual(expected);
  });

  test("Given SearchByTitle, should return the list of cards ids", () => {
    const actual: SearchByTitle[] = [
      {
        title: "The Martian",
        id: "17",
      },
      { title: "two", id: "2" },
      { title: "threE", id: "3" },
    ];

    let expected: string[] = [];

    expect(getSearchResult(actual, "xyz")).toStrictEqual(expected);

    expect(actual[0].title).toBe("The Martian");
    //
    expected = ["17"];
    expect(getSearchResult(actual, "the martian")).toStrictEqual(expected);
    //
    expected = ["17", "2", "3"];
    expect(getSearchResult(actual, "t")).toStrictEqual(expected);

    expected = ["17", "3"];
    expect(getSearchResult(actual, "thr")).toStrictEqual(expected);
  });

  test("Given the ids should return the chunks of specified length", () => {
    //
    const actual = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"];

    expect(actual).toStrictEqual([
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "10",
      "11",
    ]);

    const expected = [
      ["1", "2", "3", "4", "5"],
      ["6", "7", "8", "9", "10"],
      ["11"],
    ];

    const chunksNumber = 5;
    expect(getChunks(actual, chunksNumber)).toStrictEqual(expected);
  });

  test("Given a chunk List, must give back even chunks after odd chunks in a flat list", () => {
    const actual = [
      ["1", "2", "3", "4", "5"],
      ["6", "7", "8", "9", "10"],
      ["11", "12", "13", "14", "15"],
      ["16", "17", "18", "19", "20"],
      ["21"],
    ];

    expect(actual).toStrictEqual([
      ["1", "2", "3", "4", "5"],
      ["6", "7", "8", "9", "10"],
      ["11", "12", "13", "14", "15"],
      ["16", "17", "18", "19", "20"],
      ["21"],
    ]);

    getOddEvenOrderedList(actual);
    const expected = [
      "1",
      "2",
      "3",
      "4",
      "5",
      "11",
      "12",
      "13",
      "14",
      "15",
      "21",
      "6",
      "7",
      "8",
      "9",
      "10",
      "16",
      "17",
      "18",
      "19",
      "20",
    ];
    expect(getOddEvenOrderedList(actual)).toStrictEqual(expected);
  });

  test("Given Ids and search criteria, it should return the ordered list.", () => {
    const chunksNumber = 5;
    const searchList: SearchByTitle[] = [
      {
        id: "1",
        title: "American History X",
      },
      {
        id: "2",
        title: "Pirates of the Caribbean: At World's End",
      },
      {
        id: "12",
        title: "Pirates of the Caribbean: X",
      },
    ];

    const props = {
      cards: FakeState,
      searchList,
      searchValue: "x",
      chunksNumber,
      isSearch: true,
    };

    const actual = FakeState.ids;
    const expected = { ids: ["12", "1"] };

    expect(actual).toStrictEqual(["1", "2", "12"]);

    expect(getOrderedCards(props)).toStrictEqual(expected);
  });
});

import { MovieData } from "./jsonDecoder";
import { Cards } from "../types";

export function transformMovieData(data: MovieData[]): Cards {
  const one = 1;
  return data.reduce(
    (acc: Cards, value, idx) => {
      const id = String(idx + one);

      const cards = {
        [id]: {
          id,
          description: value.description,
          image: value.images.posterArt.url,
          releaseYear: value.releaseYear,
          title: value.title,
        },
      };

      return {
        ...acc,
        ids: [...acc.ids, id],
        data: {
          ...acc.data,
          ...cards,
        },
      };
    },
    {
      ids: [],
      data: {},
    }
  );
}

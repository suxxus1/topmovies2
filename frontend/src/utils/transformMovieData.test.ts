import { transformMovieData } from "./";

import { MovieData } from "../utils/jsonDecoder";
import { Cards } from "../types";

const movieData: MovieData = {
  title: "Lorem",
  description: "desc",
  images: {
    posterArt: { url: "http..." },
  },
  releaseYear: 1999,
};

const c: Cards = {
  ids: ["1", "2"],
  data: {
    "1": {
      id: "1",
      title: movieData.title,
      description: movieData.description,
      image: movieData.images.posterArt.url,
      releaseYear: movieData.releaseYear,
    },
    "2": {
      id: "2",
      title: movieData.title,
      description: movieData.description,
      image: movieData.images.posterArt.url,
      releaseYear: movieData.releaseYear,
    },
  },
};

describe("Transform fetched data", () => {
  test("Given MovieData should return Cards", () => {
    const actual: MovieData[] = [{ ...movieData }, { ...movieData }];

    /* eslint-disable */
    expect(actual.at(0)).toStrictEqual(movieData);
    expect(actual.at(1)).toStrictEqual(movieData);

    const expected = { ...c };
    expect(transformMovieData(actual)).toStrictEqual(expected);
  });
});

import { moviesDataDecoder } from "./";
import { Nothing } from "../types";

const entry = {
  title: "American History X",
  description:
    "A former neo-nazi skinhead tries to prevent his younger brother from going down the same wrong path that he did.",
  images: {
    posterArt: {
      url: "https://static.rviewer.io/challenges/datasets/dreadful-cherry-tomatoes/assets/american_history_x.jpg",
      width: 497,
      height: 736,
    },
  },
  releaseYear: 1998,
};

const movieData = {
  entries: [{ ...entry }],
};

describe("Data decoders", () => {
  test("Given a JSON should return MovieData", () => {
    const data = moviesDataDecoder({
      entries: [...movieData.entries],
    });

    const hasData = data !== Nothing;

    if (hasData) {
      const { title, description, releaseYear, images } = data[0];

      expect(title).toBe(entry.title);
      expect(description).toBe(entry.description);
      expect(releaseYear).toBe(entry.releaseYear);
      expect(images.posterArt.url).toBe(entry.images.posterArt.url);
    }
  });

  test("Given a JSON file, it should throw an Error message when one of the properties is not as expected.", () => {
    const actual = {
      ...entry,
      releaseYear: null,
    };

    expect(actual.releaseYear).toBe(null);

    console.error = jest.fn();

    const data = moviesDataDecoder({
      entries: [{ ...actual }],
    });

    expect(console.error).toHaveBeenCalled();
    expect(data).toBe(Nothing);
  });
});

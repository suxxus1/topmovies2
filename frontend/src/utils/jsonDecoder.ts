import * as D from "json-decoder";
import { Nothing, Maybe } from "../types";

type PosterArt = {
  url: string;
};

type Images = {
  posterArt: PosterArt;
};

export type MovieData = {
  title: string;
  description: string;
  images: Images;
  releaseYear: number;
};

const posterArtDecoder = D.objectDecoder<PosterArt>({
  url: D.stringDecoder,
});

const imagesDecoder = D.objectDecoder<Images>({
  posterArt: posterArtDecoder,
});

const movieDecoder = D.objectDecoder<MovieData>({
  title: D.stringDecoder,
  description: D.stringDecoder,
  images: imagesDecoder,
  releaseYear: D.numberDecoder,
});

export function moviesDataDecoder(data: unknown): Maybe<MovieData[]> {
  const decoded = D.objectDecoder({
    entries: D.arrayDecoder(movieDecoder),
  }).decode(data);

  switch (decoded.type) {
    case "ERR":
      console.error(decoded.message);
      return Nothing;
    case "OK":
      return decoded.value.entries;
  }
}

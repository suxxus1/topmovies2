import React, { useMemo, useEffect } from "react";
import styled from "styled-components";
import {
  useCardsContext,
  useSearchBoxContext,
  usePaginationContext,
} from "../../context";
import { getCloned, getOrderedCards, createSearchList } from "../../utils";
import constants from "../../constants";
import { Cards, CardId, SearchByTitle } from "../../types";
import { device } from "../../styles/globalStyles";
import { iso } from "../../styles/AppStyles";
import CardsGrid from "../cards/Cards";
import Pagination from "../pagination/Pagination";

const Iso = styled.div`
  background-repeat: no-repeat;
  background-position: center;
  width: 200px;
  height: 200px;
  margin: 0 auto;
  opacity: 0.3;
  ${iso}

  @media (${device.tablet}) {
    width: 100%;
  }
`;

const Wrapper = styled.div`
  margin-bottom: 50px;

  .pagination {
    margin: 0 auto;
    margin-top: 10px;
  }
`;

const Title = styled.h2`
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 27px;

  @media (${device.tablet}) {
    padding: 0;
  }
`;

const PopularMovies = styled.div`
  margin: 25px auto 27px;

  width:100%;
  height: 100%;

  @media (${device.tablet}) {
      width: 1092px;
  }
}
`;

function getPageNum(ids: CardId[], cardsPerPage: number): number {
  return Math.ceil(ids.length / cardsPerPage);
}

export default function Movies(): JSX.Element {
  const { ids, data } = useCardsContext();
  const { searchValue } = useSearchBoxContext();
  const { setPageNum } = usePaginationContext();

  const searchList = useMemo<SearchByTitle[]>(
    () => createSearchList(getCloned({ ids, data }) as Cards),
    [ids, data]
  );

  const orderedCards = useMemo(() => {
    return getOrderedCards({
      cards: { ids, data },
      searchList,
      searchValue,
      chunksNumber: constants.CARDS_PER_ROW,
      isSearch: searchValue.length >= constants.SEARCH_CRITERIA_MIN_LENGTH,
    });
  }, [data, ids, searchList, searchValue]);

  const noResults = !orderedCards.ids.length;

  useEffect(() => {
    if (orderedCards.ids.length) {
      setPageNum(getPageNum(orderedCards.ids, constants.MAX_CARDS_PER_PAGE));
    }
  }, [orderedCards.ids, setPageNum]);

  if (noResults) {
    return <Iso />;
  }

  return (
    <Wrapper>
      <PopularMovies data-testid="popular-movies">
        <Title data-testid="main-title">Popular Movies</Title>
        <CardsGrid ids={orderedCards.ids} />
      </PopularMovies>
      <Pagination />
    </Wrapper>
  );
}

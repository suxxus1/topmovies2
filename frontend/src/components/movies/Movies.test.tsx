import React from "react";
import { render, screen } from "../testUtils";
import Movies from "./Movies";
import { FakeMovies, getFakeMovies } from "../../mocks";

describe("<Movies> ", () => {
  test("Render correctly", () => {
    const startIndex = 0;
    const upToTwenty = 20;

    render(<Movies />, {
      route: "/welcome",
      data: getFakeMovies(startIndex, upToTwenty, FakeMovies),
    });

    screen.getByTestId("popular-movies");
    screen.getByText(/Popular Movies/i);
    screen.getByTestId("cards-container");
    screen.getAllByTestId("card-container");
    screen.getByTestId("paginator");

    screen.queryAllByRole("button");
  });
});

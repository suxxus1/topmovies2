import type { Meta, StoryObj } from "@storybook/react";
import Movies from "./Movies";

const movies: Meta<typeof Movies> = {
  title: "Components/Movies",
  component: Movies,
};

export default movies;

type Story = StoryObj<typeof Movies>;

export const Basic: Story = {};

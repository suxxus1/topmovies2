import React from "react";
import { render, screen } from "../testUtils";
import Loader from "./Loader";

describe("<Loader> ", () => {
  test("Render correctly", () => {
    render(<Loader />, {});
    screen.getByTestId("loading-iso");
    screen.getByText("Loading...");
  });
});

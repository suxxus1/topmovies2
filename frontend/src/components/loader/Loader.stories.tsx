import type { Meta, StoryObj } from "@storybook/react";
import Loader from "./Loader";
import { getFetchMock } from "../../mocks";

const loader: Meta<typeof Loader> = {
  title: "Components/Loader",
  component: Loader,
};

export default loader;

type Story = StoryObj<typeof Loader>;

export const Basic: Story = {};

export const LoadedWithErrors: Story = {
  parameters: {
    fetchMock: getFetchMock([{}]),
  },
};

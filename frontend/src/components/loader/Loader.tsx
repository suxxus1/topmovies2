import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import styled, { keyframes } from "styled-components";
import { device } from "../../styles/globalStyles";
import { Iso } from "../../styles/AppStyles";
import { useFetchDataMoviesContext } from "../../context";
import { Nothing } from "../../types";
import constants from "../../constants";
import ErrorMsg from "../errorMsg/ErrorMsg";

const fadeInFadeOut = keyframes`
  0% {
    opacity: 1;
  }
  50% {
    opacity: 0.2;
  }
  100% {
    opacity: 1;
  }
`;

const LoadingDataIso = styled(Iso)`
  animation-name: ${fadeInFadeOut};
  animation-duration: 5s;
  animation-iteration-count: infinite;
  margin-bottom: 40px;
`;

const LoadingData = styled.div`
  text-align: center;
  @media (${device.tablet}) {
    .error-message {
      max-width: 50%;
      margin: auto;
    }
  }
`;

export default function Loader(): JSX.Element {
  const { isLoading, data } = useFetchDataMoviesContext();
  const navigate = useNavigate();

  const validJson = data !== Nothing;
  const loadedNotValidJson = !isLoading && !validJson;
  const loadedValidJson = !isLoading && validJson;

  useEffect(() => {
    if (isLoading) {
      navigate("/", { replace: true });
    }

    if (loadedValidJson) {
      navigate("/welcome", { replace: true });
    }
  }, [isLoading, navigate, loadedValidJson]);

  return (
    <LoadingData>
      {loadedNotValidJson ? (
        <ErrorMsg msg={constants.NOT_VALID_JSON_MSG} />
      ) : (
        <>
          <LoadingDataIso data-testid="loading-iso" />
          <p>Loading...</p>
        </>
      )}
    </LoadingData>
  );
}

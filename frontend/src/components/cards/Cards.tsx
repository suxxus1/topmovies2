import React, { useMemo } from "react";
import styled from "styled-components";
import { device } from "../../styles/globalStyles";
import Card from "../card/Card";
import { usePaginationContext, useCardsContext } from "../../context";
import { Cards, CardId } from "../../types";
import { getValidCards } from "../../utils";
import constants from "../../constants";

const Wrapper = styled.div<{ $colsNum: number; $cardsWidth: number }>`
  .cards {
    margin: 0 auto;
    display: grid;
    gap: 15px;
    transition: all 1s ease;
  }

  @media (${device.tablet}) {
    .cards {
      width: ${(props) => props.$cardsWidth}px;
      grid-template-columns: repeat(${(props) => props.$colsNum}, 1fr);
    }
  }
`;

const Mask = styled.div<{ $maskWidth: number }>`
  @media (${device.tablet}) {
    width: ${(props) => `${props.$maskWidth}px`};
    overflow: hidden;
  }
`;

export default function CardsGrid({ ids }: { ids: CardId[] }): JSX.Element {
  const rows = 2;

  const { data } = useCardsContext();
  const { pageSelected } = usePaginationContext();

  const cardWidth = 220;

  const colsNum = Math.ceil(ids.length / rows);
  const cardsContainerWidth = colsNum * cardWidth;
  const maskWidth = cardWidth * constants.CARDS_PER_ROW;

  const validCards = useMemo<Cards>(
    () =>
      getValidCards({
        ids,
        data,
      }),
    [ids, data]
  );
  return (
    <Wrapper
      $colsNum={colsNum}
      $cardsWidth={cardsContainerWidth}
      data-testid="cards-container"
    >
      <Mask $maskWidth={maskWidth}>
        <div
          className="cards"
          style={{
            transform: `translateX(${maskWidth * (pageSelected - 1) * -1}px)`, // eslint-disable-line no-magic-numbers
          }}
        >
          {validCards.ids.map((id) => (
            <div key={`card_${id}`} className="card">
              <Card {...validCards.data[id]} />
            </div>
          ))}
        </div>
      </Mask>
    </Wrapper>
  );
}

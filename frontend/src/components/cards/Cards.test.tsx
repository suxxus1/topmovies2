import React from "react";
import { render, screen } from "../testUtils";
import Cards from "./Cards";
import { FakeMovies, defaultEntry } from "../../mocks";

const { entries } = FakeMovies;

/* eslint-disable no-magic-numbers */
const one = entries.at(0) || defaultEntry;
const two = entries.at(1) || defaultEntry;
const three = entries.at(2) || defaultEntry;

describe("<Cards> ", () => {
  test("Render correctly", () => {
    render(<Cards ids={[]} />, {});
    //
    expect(screen.queryByText(one.title)).not.toBeInTheDocument();
    expect(screen.queryByText(one.description)).not.toBeInTheDocument();
    expect(screen.queryByText(one.releaseYear)).not.toBeInTheDocument();
    expect(screen.queryByText(two.title)).not.toBeInTheDocument();
    expect(screen.queryByText(three.title)).not.toBeInTheDocument();
    //
    render(<Cards ids={["1", "2", "3"]} />, {
      data: FakeMovies,
    });

    screen.getAllByText(one.title);
    screen.getAllByText(one.description);
    screen.getAllByText(one.releaseYear);
    screen.getByText(two.title);
    screen.getByText(three.title);
  });
});

import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Cards from "./Cards";
import { usePaginationContext } from "../../context";

const numberOfCards = 20;
const one = 1;

const cards: Meta<typeof Cards> = {
  title: "Components/Cards",
  component: Cards,
  args: {
    ids: Array(numberOfCards)
      .fill("")
      .map((_, i) => (i + one).toString()),
  },
};

export default cards;

type Story = StoryObj<typeof Cards>;

export const Basic: Story = {};

export const PageTwoWasSelected: Story = {
  decorators: [
    (Story) => {
      const { setPageSeleted } = usePaginationContext();
      const pageNumberTwo = 2;

      setPageSeleted(pageNumberTwo);

      return <Story />;
    },
  ],
};

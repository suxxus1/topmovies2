import type { Meta, StoryObj } from "@storybook/react";
import ErrorMsg from "./ErrorMsg";

const errorMsg: Meta<typeof ErrorMsg> = {
  title: "Components/ErrorMsg",
  component: ErrorMsg,
};

export default errorMsg;

type Story = StoryObj<typeof ErrorMsg>;

export const Basic: Story = {
  args: {
    msg: "Lorem ipsum",
  },
};

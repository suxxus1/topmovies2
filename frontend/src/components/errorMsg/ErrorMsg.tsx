import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  padding: 10px;
  text-align: center;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

  p {
    font-size: 12px;
    margin: 20px;
    padding: 6px;
    background-color: var(--red-bg-50);
    color: white;
  }
`;

export default function ErrorMsg({ msg }: { msg: string }): JSX.Element {
  return (
    <Wrapper data-testid="error-message" className="error-message">
      <h3>Sorry, something went wrong.</h3>
      <p>{msg}</p>
    </Wrapper>
  );
}

import React from "react";
import { render, screen } from "../testUtils";
import ErrorMsg from "./ErrorMsg";

const title = "Sorry, something went wrong.";

describe("<ErrorMsg> ", () => {
  test("Render correctly", () => {
    const msg = "lorem ipsum";
    render(<ErrorMsg msg={msg} />, {});

    screen.getByText(title);
    screen.getByText(msg);
  });
});

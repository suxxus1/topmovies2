import type { Meta, StoryObj } from "@storybook/react";
import Footer from "./Footer";

const footer: Meta<typeof Footer> = {
  title: "Components/Footer",
  component: Footer,
};

export default footer;

type Story = StoryObj<typeof Footer>;

export const Basic: Story = {};

import React from "react";
import { render, screen } from "../testUtils";
import Footer from "./Footer";

describe("<Footer> ", () => {
  test("Render correctly", () => {
    const appStoreHref = "https://www.apple.com/app-store/";
    const googlePlayHref = "https://play.google.com/store/";
    const copyTxt = "© Copyright 2022 Dreadful Tomatoes. All rights reserved.";

    render(<Footer />, {});
    screen.getByTestId("logo");

    expect(screen.getByTestId("app-store")).toHaveAttribute(
      "href",
      appStoreHref
    );

    expect(screen.getByTestId("google-play")).toHaveAttribute(
      "href",
      googlePlayHref
    );

    screen.getByText(copyTxt);
  });
});

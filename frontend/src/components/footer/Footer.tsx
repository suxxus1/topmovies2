import React from "react";
import styled from "styled-components";
import { logo, appStore, googlePlay } from "../../styles/AppStyles";

const Footer = styled.footer`
  height: 230px;
  padding: 27px;
  background-color: var(--top-bottom-bg);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

const Logo = styled.div`
  background-repeat: no-repeat;
  width: 210px;
  height: 50px;
  ${logo}
`;

const GooglePlay = styled.a`
  background-repeat: no-repeat;
  width: 125px;
  height: 41px;
  ${googlePlay}
`;

const AppStore = styled.a`
  background-repeat: no-repeat;
  width: 125px;
  height: 41px;
  ${appStore}
`;

const Copy = styled.p`
  font-size: 12px;
`;

const Store = styled.div`
  display: flex;
  gap: 8px;
`;

export default function Bottom() {
  return (
    <Footer data-testid="footer-container">
      <Logo data-testid="logo" />
      <Store>
        <AppStore
          data-testid="app-store"
          href="https://www.apple.com/app-store/"
          target="_blank"
        />
        <GooglePlay
          data-testid="google-play"
          href="https://play.google.com/store/"
          target="_blank"
        />
      </Store>
      <Copy>© Copyright 2022 Dreadful Tomatoes. All rights reserved.</Copy>
    </Footer>
  );
}

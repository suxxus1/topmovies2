import React from "react";
import { render, screen } from "../testUtils";
import Card from "./Card";
import { FakeState } from "../../mocks";
import { MovieCard } from "../../types";

const movieCard: MovieCard = FakeState.data["1"];

const { title, releaseYear, description, image } = movieCard;

describe("<Card> ", () => {
  test("Render correctly", () => {
    render(<Card {...movieCard} />, {});
    screen.getByTestId("card-1");
    screen.getByText(title);
    screen.getByText(releaseYear);
    screen.getByText(description);

    const displayedImage = document.querySelector("img") as HTMLImageElement;
    expect(displayedImage.src).toContain(image);
    expect(displayedImage.alt).toContain(title);
  });
});

import type { Meta, StoryObj } from "@storybook/react";
import Card from "./Card";

import { FakeState } from "../../mocks";
import { MovieCard } from "../../types";

const card: Meta<typeof Card> = {
  title: "Components/Card",
  component: Card,
};

export default card;

type Story = StoryObj<typeof Card>;

const movieCard: MovieCard = FakeState.data["2"];
const movieCardWithLongTitle: MovieCard = FakeState.data["12"];

export const Basic: Story = {
  args: {
    ...movieCard,
  },
};

export const CardWithLongTitle: Story = {
  args: {
    ...movieCardWithLongTitle,
  },
};

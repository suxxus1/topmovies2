import React from "react";
import styled from "styled-components";
import { MovieCard } from "../../types";

const Wrapper = styled.div`
  position: relative;
  overflow: hidden;
  height: 100%;
  width: fit-content;
  margin: 0 auto;
`;

const MovieCardInfoWrapper = styled.div`
  background-color: #000;
  opacity: 0.8;
  position: absolute;
  padding: 21px;
  height: 290px;
  bottom: -215px;
  transition: all 1s ease;

  :hover {
    bottom: 0;
  }
`;

const Info = styled.ul`
  font-size: 12px;

  li {
    margin-bottom: 14px;

    -webkit-box-orient: vertical;
    white-space: normal;
    text-overflow: ellipsis;
    overflow: hidden;
    display: -webkit-box !important;
  }

  > :first-child {
    height: 40px;
    line-height: 20px;
    margin-bottom: 30px;
    font-weight: bold;
    font-size: 14px;
    -webkit-line-clamp: 2;
  }

  > :last-child {
    max-height: 138px;
    -webkit-line-clamp: 8;
  }
`;

export default function Card({
  id,
  image,
  title,
  description,
  releaseYear,
}: MovieCard): JSX.Element {
  return (
    <Wrapper data-testid="card-container">
      <MovieCardInfoWrapper data-testid={`card-${id}`} className="card-info">
        <Info data-testid="card-info">
          <li>{title}</li>
          <li>{releaseYear}</li>
          <li>{description}</li>
        </Info>
      </MovieCardInfoWrapper>
      <img alt={title} src={image} />
    </Wrapper>
  );
}

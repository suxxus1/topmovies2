import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import MainView from "./";
import { getFetchMock } from "../../mocks";

const mainView: Meta<typeof MainView> = {
  title: "App/MainView",
  component: MainView,
};

export default mainView;

type Story = StoryObj<typeof MainView>;

export const Basic: Story = {
  parameters: {
    reactRouter: {
      browserPath: "/",
    },
  },
};

export const FectchedJsonHasErrors: Story = {
  parameters: {
    reactRouter: {
      browserPath: "/",
    },
    fetchMock: getFetchMock([{}]),
  },
};

export const WhenThereIsNoMatchForTheSearchCriteria: Story = {
  parameters: {
    reactRouter: {
      browserPath: "/welcome",
    },
    fetchMock: getFetchMock([]),
  },
};

export const WithWrongUrl: Story = {
  parameters: {
    reactRouter: {
      browserPath: "/wrong-url",
    },
  },
};

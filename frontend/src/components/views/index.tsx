import React from "react";
import { Route, Routes } from "react-router-dom";
import styled from "styled-components";
import { device } from "../../styles/globalStyles";
import { Iso } from "../../styles/AppStyles";

import Loader from "../loader/Loader";
import Header from "../header/Header";
import SearchBox from "../searchBox/SearchBox";
import Movies from "../movies/Movies";
import Footer from "../footer/Footer";

const Wrapper = styled.div`
  background-color: var(--primary-bg);
  max-width: 1441px;
  margin: auto;
`;

const Container = styled.div`
  min-height: 350px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 0 30px;

  @media (${device.tablet}) {
    min-height: 750px;
  }
`;

export default function MainView(): JSX.Element {
  return (
    <Wrapper data-testid="main-wrapper">
      <Header />
      <SearchBox />
      <Container data-testid="routes-container">
        <Routes>
          <Route path="/" element={<Loader />} />
          <Route path="/welcome" element={<Movies />} />
          <Route path="/*" element={<Iso />} />
        </Routes>
      </Container>
      <Footer />
    </Wrapper>
  );
}

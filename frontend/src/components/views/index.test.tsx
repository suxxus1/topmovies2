import React from "react";
import { render, screen } from "../testUtils";
import MainView from "./";
import { FakeMovies } from "../../mocks";

describe("<MainView> ", () => {
  test("Render correctly", () => {
    render(<MainView />, {
      route: "/welcome",
      data: FakeMovies,
    });

    screen.getByTestId("header");
    screen.getByTestId("searchbox");
    screen.getByTestId("popular-movies");
    screen.getByText(/Popular Movies/i);
    screen.getByTestId("cards-container");
    screen.getAllByTestId("card-container");
    screen.getByTestId("paginator");
    screen.getByTestId("footer-container");
  });
});

import React from "react";
import styled from "styled-components";
import { logo } from "../../styles/AppStyles";
import { device } from "../../styles/globalStyles";

const Wrapper = styled.header`
  height: 62px;
  padding: 6px 5%;
  background-color: var(--top-bottom-bg);

  @media (${device.tablet}) {
    padding-left: 12%;
  }
`;

const Logo = styled.div`
  background-repeat: no-repeat;
  width: 100%;
  height: 100%;
  ${logo}
`;

export default function Header() {
  return (
    <Wrapper data-testid="header">
      <Logo data-testid="header-logo" />
    </Wrapper>
  );
}

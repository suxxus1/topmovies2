import React, { useState, useEffect } from "react";
import { useDebounce } from "usehooks-ts";
import styled from "styled-components";
import { magnifier } from "../../styles/AppStyles";
import { useSearchBoxContext, usePaginationContext } from "../../context";
import constants from "../../constants";

const Wrapper = styled.div`
  display: flex;
  height: 68px;
  background-color: var(--red-bg-50);
  padding: 14px 5%;
`;

const InputBox = styled.input`
  background-repeat: no-repeat;
  ${magnifier}
  background-size: 20px;
  background-position-y: 50%;
  background-position-x: 30px;
  width: 100%;
  height: 100%;
  background-color: white;
  padding-left: 50px;
  border-radius: 5px;
  color: var(--primary-bg);
  max-width: 1092px;
  margin: auto;

  ::placeholder {
    color: var(--placeholder);
  }
`;

export default function SearchBox(): JSX.Element {
  const delay = 500;
  const { setSearchValue } = useSearchBoxContext();
  const [value, setValue] = useState<string>("");
  const { setPageSeleted } = usePaginationContext();
  const debouncedValue = useDebounce<string>(value, delay);

  const onChangeHandler = (evt: React.ChangeEvent<HTMLInputElement>) => {
    evt.preventDefault();
    setValue(evt.target.value);
  };

  useEffect(() => {
    setSearchValue(debouncedValue);
    setPageSeleted(constants.STARTING_PAGE);
  }, [debouncedValue, setSearchValue, setPageSeleted]);

  return (
    <Wrapper>
      <InputBox
        type="text"
        placeholder="Title"
        value={value}
        onChange={onChangeHandler}
        aria-label="search by title"
        data-testid="searchbox"
      />
    </Wrapper>
  );
}

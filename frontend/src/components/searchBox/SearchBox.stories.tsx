import type { Meta, StoryObj } from "@storybook/react";
import SearchBox from "./SearchBox";

const searchBox: Meta<typeof SearchBox> = {
  title: "Components/SearchBox",
  component: SearchBox,
};

export default searchBox;

type Story = StoryObj<typeof SearchBox>;

export const Basic: Story = {};

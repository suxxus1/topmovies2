import React from "react";
import { render, screen, fireEvent } from "../testUtils";
import SearchBox from "./SearchBox";

const searchInputSetup = () => {
  const utils = render(<SearchBox />, {});
  const input: HTMLInputElement = screen.getByTestId("searchbox");
  return {
    input,
    ...utils,
  };
};

describe("<SearchBox /> ", () => {
  test("Render correctly", () => {
    render(<SearchBox />, {});
    screen.getByTestId("searchbox");
  });

  test("The user enter text", () => {
    const input = searchInputSetup().input;

    fireEvent.change(input, { target: { value: "xyz" } });
    expect(input.value).toBe("xyz");

    fireEvent.change(input, { target: { value: "" } });
    expect(input.value).toBe("");
  });
});

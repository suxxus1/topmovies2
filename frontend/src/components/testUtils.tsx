import React, { ReactElement } from "react";
import * as usehooks from "usehooks-ts";
import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import {
  FetchDataMoviesContextProvider,
  CardsContextProvider,
  PaginationContextProvider,
  SearchBoxContextProvider,
} from "../context/";

export type Props = {
  route: string;
  data?: unknown;
  error?: Error;
};

function customRender(
  ui: ReactElement,
  {
    route = "",
    data = { entries: [] },
    error = undefined,
    ...renderOptions
  }: Partial<Props>
) {
  const spy = jest.spyOn(usehooks, "useFetch");
  spy.mockReturnValue({
    data,
    error,
  });

  function wrapper({ children }: { children: React.ReactNode }) {
    return (
      <FetchDataMoviesContextProvider>
        <CardsContextProvider>
          <PaginationContextProvider>
            <SearchBoxContextProvider>
              <MemoryRouter initialEntries={[route]}>{children}</MemoryRouter>
            </SearchBoxContextProvider>
          </PaginationContextProvider>
        </CardsContextProvider>
      </FetchDataMoviesContextProvider>
    );
  }

  return render(ui, { wrapper, ...renderOptions });
}

export * from "@testing-library/user-event";
export * from "@testing-library/react";
export { customRender as render };

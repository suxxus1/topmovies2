import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { device } from "../../styles/globalStyles";
import { usePaginationContext, useSearchBoxContext } from "../../context";
import { chevron } from "../../styles/AppStyles";
import constants from "../../constants";

const Paginator = styled.button`
  align-items: center;
  cursor: default;
  padding: 5px;
  display: none;

  @media (${device.tablet}) {
    display: flex;
  }
`;

const Pages = styled.ul`
  display: flex;
  gap: 9px;
  transition: all 1s ease;
`;

const Page = styled.li``;

const PaginationButtons = styled.div<{ $selected: boolean }>`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 12px;
  font-weight: bold;
  background-color: ${(props) =>
    `var(--red-bg-${props.$selected ? "50" : "60"})`};
  border-radius: 50%;
  width: 30px;
  height: 30px;
  cursor: pointer;

  &:hover {
    background-color: var(--red-bg-50);
  }
`;

const Chevron = styled.div<{ $show: boolean }>`
  background-repeat: no-repeat;
  ${chevron};
  visibility: ${(props) => `${props.$show ? "visible" : "hidden"}`};
  pointer-events: ${(props) => `${props.$show ? "fill" : "none"}`};
  width: 24px;
  height: 24px;
  cursor: pointer;
`;

const ChevronL = styled(Chevron)`
  transform: rotate(180deg);
`;

const Mask = styled.div<{ $maskWidth: number }>`
  width: ${(props) => props.$maskWidth}px;
  overflow: hidden;
`;

function PageNum({
  order,
  isSelected,
}: {
  order: number;
  isSelected: boolean;
}): JSX.Element {
  const label = String(order);
  return (
    <PaginationButtons
      role="button"
      data-testid={`pageNum-${order.toString()}${
        isSelected ? "-selected" : ""
      }`}
      id={order.toString()}
      $selected={isSelected}
    >
      {label}
    </PaginationButtons>
  );
}

function getMaskWidhth(pageNum: number) {
  const pageButtonWidth = 30;
  const buffer = 9;
  const maxButtonsPerSlide = 5;
  const one = 1;

  return pageNum >= maxButtonsPerSlide
    ? maxButtonsPerSlide * pageButtonWidth + (maxButtonsPerSlide - one) * buffer
    : pageNum * pageButtonWidth + (pageNum - one) * buffer;
}

export default function Pagination(): JSX.Element {
  const intialSlideX = 0;
  const one = 1;

  const { pageSelected, pageNum, setPageSeleted } = usePaginationContext();
  const { searchValue } = useSearchBoxContext();

  const [slideX, setSlideX] = useState<number>(intialSlideX);
  const [slideNum, setSlideNum] = useState<number>(constants.STARTING_PAGE);

  const slideWidth = 195;

  const slideChunks = pageNum / constants.CARDS_PER_ROW;

  useEffect(() => {
    if (searchValue.length >= constants.SEARCH_CRITERIA_MIN_LENGTH) {
      setSlideX(intialSlideX);
      setSlideNum(constants.STARTING_PAGE);
    }
  }, [searchValue]);

  return (
    <Paginator
      className="pagination"
      data-testid="paginator"
      onClick={(evt) => {
        evt.stopPropagation();

        const target = (evt.target as HTMLDivElement).id;
        const isPageL = target === "page-left";
        const isPageR = target === "page-right";

        /* eslint-disable  no-magic-numbers */
        switch (true) {
          case !target:
            break;
          case isPageL:
            setSlideX((slideNum - 2) * slideWidth * -1);
            setSlideNum((n) => n - 1);
            break;
          case isPageR:
            setSlideX(slideNum * slideWidth * -1);
            setSlideNum((n) => n + 1);
            break;
          default:
            setPageSeleted(parseInt(target, 10) || -1);
        }
        /* eslint-enable  no-magic-numbers */
      }}
    >
      <ChevronL data-testid="page-left" $show={slideNum > one} id="page-left" />
      <Mask data-testid="pages-container" $maskWidth={getMaskWidhth(pageNum)}>
        <Pages
          data-testid="pagination"
          style={{ transform: `translateX(${slideX}px)` }}
        >
          {Array(pageNum)
            .fill("")
            .map((_, idx) => idx + one)
            .map((n) => (
              <Page key={`item_${n}`}>
                <PageNum
                  {...{
                    order: n,
                    isSelected: pageSelected === n,
                  }}
                />
              </Page>
            ))}
        </Pages>
      </Mask>
      <Chevron
        data-testid="page-right"
        $show={slideNum < slideChunks}
        id="page-right"
      />
    </Paginator>
  );
}

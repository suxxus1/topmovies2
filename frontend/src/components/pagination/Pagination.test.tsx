import React from "react";
import { render, screen } from "../testUtils";
import Pagination from "./Pagination";

describe("<Pagination> ", () => {
  test("Render correctly", () => {
    render(<Pagination />, {});
    screen.getByTestId("paginator");
    screen.getByTestId("pagination");
    screen.getByTestId("page-left");
    screen.getByTestId("page-right");
    expect(screen.queryByRole("button")).not.toBeInTheDocument();
  });
});

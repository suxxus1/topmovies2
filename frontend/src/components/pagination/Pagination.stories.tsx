import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Pagination from "./Pagination";
import { usePaginationContext } from "../../context";
import { FakeMovies, getFetchMock } from "../../mocks";

const fromStart = 0;
const upToTen = 10;
const upToFifty = 50;
const upToSixty = 60;

function getMockMovies(startIndex: number, endIndex: number) {
  return getFetchMock(FakeMovies.entries.slice(startIndex, endIndex));
}

const pagination: Meta<typeof Pagination> = {
  title: "Components/Pagination",
  component: Pagination,
};

export default pagination;

type Story = StoryObj<typeof Pagination>;

export const OnePage: Story = {
  parameters: {
    fetchMock: {
      ...getMockMovies(fromStart, upToTen),
    },
  },
};

export const FivePages: Story = {
  parameters: {
    fetchMock: {
      ...getMockMovies(fromStart, upToFifty),
    },
  },
};

export const SixPages: Story = {
  parameters: {
    fetchMock: {
      ...getMockMovies(fromStart, upToSixty),
    },
  },
};

export const PageNumberThreeSelected: Story = {
  decorators: [
    (Story) => {
      const { setPageSeleted } = usePaginationContext();
      const pageNumberThree = 3;
      setPageSeleted(pageNumberThree);
      return <Story />;
    },
  ],
};

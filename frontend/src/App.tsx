import React from "react";
import { GlobalStyles } from "./styles/globalStyles";
import MainView from "./components/views";
import {
  FetchDataMoviesContextProvider,
  CardsContextProvider,
  PaginationContextProvider,
  SearchBoxContextProvider,
} from "./context";

export default function App(): JSX.Element {
  return (
    <>
      <GlobalStyles />
      <FetchDataMoviesContextProvider>
        <CardsContextProvider>
          <PaginationContextProvider>
            <SearchBoxContextProvider>
              <MainView />
            </SearchBoxContextProvider>
          </PaginationContextProvider>
        </CardsContextProvider>
      </FetchDataMoviesContextProvider>
    </>
  );
}

import type {} from "cypress";

describe("Dreadful Cherry Tomatoes", () => {
  const isProd = Cypress.env("mode") === "production";
  before(() => {
    if (isProd) {
      cy.log("production");
      const topMovies = Cypress.env("get_podcasts");
      // ----------------------------------------------------
      cy.intercept("GET", topMovies).as("topmovies");
    }
    cy.visit("/");
  });

  describe("Main view", () => {
    it("Visit the landing page", () => {
      //
      cy.visit("/");
      if (isProd) {
        // TODO find a better NRY
        cy.get("[data-testid=header").should("to.exist");
        cy.get("[data-testid=header").should("to.exist");
        cy.get("[data-testid=searchbox").should("to.exist");
        cy.get("[data-testid=popular-movies").should("to.exist");
        cy.get("[data-testid=cards-container").should("to.exist");
        cy.get("[data-testid=card-container").should("to.exist");
        cy.get("[data-testid=paginator").should("to.exist");
        cy.get("[data-testid=footer-container").should("to.exist");
        cy.get("[data-testid=main-title]").contains("Popular Movies");

        cy.get("[data-testid=searchbox]").clear();
        cy.get("[data-testid=searchbox]").type("the marti");
        cy.get("[data-testid=card-container]").should("to.exist");
        cy.get("[data-testid=card-info]").trigger("mouseover");

        cy.get("[data-testid=card-info]").contains("The Mar");
        cy.get("[data-testid=pageNum-1-selected").contains("1");
        cy.get("[data-testid=pageNum-2").should("not.exist");

        cy.get("[data-testid=searchbox]").clear();
        cy.get("[data-testid=pageNum-1-selected").should("to.exist");
        cy.get("[data-testid=pageNum-2").should("to.exist");
        cy.get("[data-testid=pageNum-3").should("to.exist");
      } else {
        cy.get("[data-testid=header").should("to.exist");
        cy.get("[data-testid=searchbox").should("to.exist");
        cy.get("[data-testid=popular-movies").should("to.exist");
        cy.get("[data-testid=cards-container").should("to.exist");
        cy.get("[data-testid=card-container").should("to.exist");
        cy.get("[data-testid=paginator").should("to.exist");
        cy.get("[data-testid=footer-container").should("to.exist");

        cy.get("[data-testid=main-title]").contains("Popular Movies");

        cy.get("[data-testid=searchbox]").clear();
        cy.get("[data-testid=searchbox]").type("the marti");
        cy.get("[data-testid=card-container]").should("to.exist");
        cy.get("[data-testid=card-info]").trigger("mouseover");
        cy.get("[data-testid=card-info]").contains("The Mar");
        cy.get("[data-testid=pageNum-1-selected").contains("1");
        cy.get("[data-testid=pageNum-2").should("not.exist");

        cy.get("[data-testid=searchbox]").clear();
        cy.get("[data-testid=pageNum-1-selected").should("to.exist");
        cy.get("[data-testid=pageNum-2").should("to.exist");
        cy.get("[data-testid=pageNum-3").should("to.exist");
        cy.get("[data-testid=pageNum-4").should("to.exist");
        cy.get("[data-testid=pageNum-5").should("to.exist");
        cy.get("[data-testid=pageNum-6").should("to.exist");
        cy.get("[data-testid=pageNum-7").should("not.exist");

        cy.get("[data-testid=page-right").click();
        cy.get("[data-testid=pageNum-6").click();
        cy.get("[data-testid=pageNum-6-selected").should("to.exist");
        cy.get("[data-testid=page-left").click();
        cy.get("[data-testid=pageNum-1").click();
      }
    });
  });
});

# Dreadful Cherry Tomatoes

<div align="center">
  <a href="https://topmovies2.onrender.com" target="_blank">
    <img src="markdown/iso.svg" width="200" height="200" />
  </a>
</div>

### Front-end technical exercise

Movies Landing page


## Demo online:

<a href="https://topmovies2.onrender.com" target="_blank">
  Dreadful Cherry Tomatoes
</a>

## Build version:

from root ./

```Docker compose up --build```

It will show a compiled version on localhost:8110


## Motivation

**The development of this app took into account:**

**_From the point of view of the user:_**

- Desktop and mobile responsive design.

**_For development_**

#### Backend

Node server to serve json files.

#### Frontend

WEBPACK + REACT + Typescript.

Development process,

- Storybook, in order to build each separate component, to test its appearance and to be able to test its integration in the application.

- Unit testing of each components with REACT Test Library.

- TDD for javascript functions development.

- JSON decoder, all received data (json) are validated, typing them to be used.

- For local development we use MSW(https://mswjs.io/) for mocking the requests that the app makes to the server.

- E2E Cypress tests.

- Docker to make a previous local deployment and check that everything works as expected, if this is correct, we deploy to a cloud service that hosts the web server + the app.

Last but not least, the documentation, so that other developers can clone the repository and use it.

***Clarification:***

The pagination, since the data supplied is made up of only 30 cards, the pager shows only 3 buttons, which would be added as more cards are available. It recommended running the develop version, ```start``` command, where more cards have been added, to see how it will play with the pagination. Storybook also displays the case with more cards.



## Develop:

./frontend/[README](frontend)

./server/[README](server)


## Technology:

- **Webpack**
- **Storybook**
- **Typescript**
- **Jest**
- **React testing library** (React component testing)
- **MSW** (to mock requests)
- **Cypress** (E2E testing)
- **Docker**
  - to develop the app
  - to run the compiled version locally (frontend & server)
  - to run Gitlab-ci locally

 [![style: styled-components](https://img.shields.io/badge/style-%F0%9F%92%85%20styled--components-orange.svg?colorB=daa357&colorA=db748e)](https://github.com/styled-components/styled-components)

## Resources:

<div>
  <img src="markdown/render.svg" width="15" height="15" />
  [render.com](https://render.com/)
</div>

